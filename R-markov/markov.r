library(markovchain)
lambda <- 5 * 10^(-6) * 3600
lambdaD <- 0.5 * lambda
DC <- 0.9
lambdaDD <- DC * lambdaD
lambdaDU <- (1 - DC) * lambdaD
beta <- 0.1
betaD <- 0.5 * beta
muDD <- 1/8
oneTransitions <- c(0,2*(1-betaD)*lambdaDD,2*(1-beta)*lambdaDU,betaD*lambdaDD,0,beta*lambdaDU)
twoTransitions <- c(muDD,0,0,lambdaDD,lambdaDU,0)
threeTransitions <- c(0,0,0,0,lambdaDD,lambdaDU)
fourTransitions <- c(0,2*muDD,0,0,0,0)
fiveTransitions <- c(0,0,2*muDD,0,0,0)
sixTransitions <- c(0,0,0,0,0,0)
states <- c("1","2","3","4","5","6")
initialState <- c(1000,0,0,0,0,0)
transitionMatrix <- matrix(c(oneTransitions,twoTransitions,threeTransitions,fourTransitions,fiveTransitions,sixTransitions),nrow=6,ncol=6,byrow=TRUE)
for (i in 1:6) {
  transitionMatrix[i,i] <- 1-sum(transitionMatrix[i,])
}
mc <- new("markovchain", states = states, transitionMatrix = transitionMatrix, name = "1oo2D")

num_states <- length(states)

layout_matrix <- matrix(ncol = 2, nrow = num_states)

for (i in 1:num_states) {
  angle <- 2 * pi * (i - 1) / num_states
  layout_matrix[i, ] <- c(cos(angle), sin(angle))
}

print(getwd())
dev.new(width = 20, height = 15, unit = "in")
plot(mc, package="diagram", layout=layout_matrix, cex=1.2, lwd=2)
dev.off()

timesteps <- 1000
df <- data.frame( "timestep" = numeric(),
 "1" = numeric(), "2" = numeric(),
 "3" = numeric(),"4" = numeric(),"5" = numeric(),"6" = numeric(), stringsAsFactors=FALSE)
 for (i in 0:timesteps) {
newrow <- as.list(c(i,round(as.numeric(initialState * mc ^ i),0)))
 df[nrow(df) + 1, ] <- newrow
 }

dev.new()
plot(df$timestep,df[,1])
points(df$timestep,df[,2], col="red")
points(df$timestep,df[,3], col="green")
points(df$timestep,df[,4], col="blue")
points(df$timestep,df[,5], col="yellow")
points(df$timestep,df[,6], col="orange")
dev.off()