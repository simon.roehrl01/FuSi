import math

def calculate_lambda(F, t):
    """
    Calculate lambda using the formula: lambda = -(ln(1 - F)) / t
    :param F: Failure probability
    :param t: Time
    :return: Lambda
    """
    if F >= 1:
        raise ValueError("Failure probability (F) should be less than 1.")
    
    # Calculate lambda
    _lambda = -math.log(1 - F) / t
    return _lambda

# Example usage:
failure_probability = [1e-4, 3e-7, 6.818e-6, 1.33e-10, 1.154e-5, 3e-6]
time = 3000  # Adjust this value accordingly


for i, F in enumerate(failure_probability):
    resulting_lambda = calculate_lambda(F, time)
    print(f"Calculated Lambda: {resulting_lambda}")
