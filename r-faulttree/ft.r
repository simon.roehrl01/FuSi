library(FaultTree)

TimeStudy <- data.frame(Exposure_Time = NULL, Prob_of_Failure = NULL)
for (power in seq(0, 8, by = 0.01))
{
  mission_time <- (3 * 10^power)
  tree <- ftree.make(type = "or", name = "Systemausfall", name2 = "fatal")

  tree <- addLogic(tree, at = 1, type = "and", name = "Leitung FSA", name2 = "Massenschluss")
  tree <- addExposed(tree, at = 2, mttf = 1 / 1.0e-10, tag = "A4", name = "Leitung FSA1", name2 = "Massenschluss")
  tree <- addExposed(tree, at = 2, mttf = 1 / 1.0e-10, tag = "A5", name = "Leitung FSA2", name2 = "Massenschluss")

  tree <- addExposed(tree, at = 1, mttf = 1 / 1.0e-10, tag = "A2", name = "Leitung Ventil-", name2 = "relais Massenschluss")
  tree <- addExposed(tree, at = 1, mttf = 1 / 2.273e-9, tag = "A3", name = "Ventilrelais ueber", name2 = "Spannungsregler nicht abs")
  tree <- addExposed(tree, at = 1, mttf = 1 / 1.0e-9, tag = "A9", name = "Kein Abschalten", name2 = "Softwarefehler")
  tree <- addExposed(tree, at = 1, mttf = 1 / 3.334e-8, tag = "A1", name = "Kontakt Ventil-", name2 = "verschweisst")

  tree <- addLogic(tree, at = 1, type = "and", name = "Keine Daten", name2 = "von MC2")
  tree <- addExposed(tree, at = 9, mttf = 1 / 1.0e-10, tag = "A51", name = "Leitung FSA2", name2 = "Massenschluss")

  tree <- addLogic(tree, at = 9, type = "or", name = "Keine Daten", name2 = "zwischen MC1 und MC2")
  tree <- addExposed(tree, at = 11, mttf = 1 / 4.437e-14, tag = "A6", name = "Prozessorfehler", name2 = "serielle Schnittstelle")
  tree <- addExposed(tree, at = 11, mttf = 1 / 3.847e-9, tag = "A7", name = "Hardwarefehler", name2 = "in MC1")
  tree <- addExposed(tree, at = 11, mttf = 1 / 3.847e-9, tag = "A8", name = "Hardwarefehler", name2 = "in MC1")

  tree <- ftree.calc(tree)
  study_row <- data.frame(Exposure_Time = mission_time, Prob_of_Failure = tree$PBF[1])
  TimeStudy <- rbind(TimeStudy, study_row)
}
# x axis label
xlab <- "Mission Time (s)"
# y axis label
ylab <- "Probability of Failure"
# plot title
main <- "Fault Tree Analysis"
# plot the data with grid lines
plot(TimeStudy, log = "x", type = "l", xlab = xlab, ylab = ylab, main = main)
# add a legend
# add a grid
grid(nx = NULL, ny = NULL, col = "grey", lty = "dotted", lwd = par("lwd"), equilogs = TRUE)
# highlight the last value
points(tree$PBF[1] ~ mission_time, col = "red", pch = 19)
# add the last value beside the point
text(tree$PBF[1] ~ mission_time, labels = tree$PBF[1], pos = 2, col = "red")
rm(mission_time)
ftree2html(tree, write_file = TRUE)
browseURL("tree.html")


## get last value of the tree
tree$PBF[1]
